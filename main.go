package main

import (
	"halalpedia-collection/db"
	"halalpedia-collection/routes"
)

func main() {
	// app := fiber.New()
	// db.Init()

	// app.Get("/", func(c *fiber.Ctx) error {
	// 	return c.SendString("Hello, World!")
	// })

	// app.Get("/pegawai", controllers.FetchAllPegawai)

	// app.Listen(":3000")
	db.Init()

	e := routes.Init()

	// e.Logger.Fatal(e.Start(":3000"))
	e.Listen(":3000")

}
