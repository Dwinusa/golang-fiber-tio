package middleware

import (
	"github.com/gofiber/fiber/v2"
)

func Headerset(c *fiber.Ctx) error {
	// result, err := models.FetchAllCollection()
	// c.Set("Public-Authorization", "Public aGFscGVkX3Byb2R1Y3Rpb25fTUszQ3ByajA0TGRQT3dLbWxka3hFMmRXMTBMczFjOQ==")
	publickey := c.Get("Public")

	if publickey == "12345" {
		return c.Status(400).JSON("header public not working")
	}

	return c.Next()
	// c.Set("Content-Type", "text/plain")
	// if err != nil {
	// return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	// return c.Status(400).JSON(http.StatusInternalServerError)
	// }
	// log.Println(result)
	// return c.Status(200).JSON(result)
}

// func auth() {
// 	app := fiber.New()
// 	app.Use(basicauth.New(basicauth.Config{
// 		Users: map[string]string{
// 			"john":  "doe",
// 			"admin": "123456",
// 		},
// 		Realm: "Forbidden",
// 		Authorizer: func(user, pass string) bool {
// 			if user == "john" && pass == "doe" {
// 				return true
// 			}
// 			if user == "admin" && pass == "123456" {
// 				return true
// 			}
// 			return false
// 		},
// 		Unauthorized: func(c *fiber.Ctx) error {
// 			return c.SendFile("./unauthorized.html")
// 		},
// 		ContextUsername: "_user",
// 		ContextPassword: "_pass",
// 	}))
// }
