package models

import (
	"encoding/base64"
	"errors"
	"log"
	"net/http"
	"regexp"
	"strings"

	"github.com/gofiber/fiber/v2"

	"halalpedia-collection/db"
)

type Headerset struct {
	Publickey  string `json:"publickey"`
	Privatekey string `json:"privatekey"`
}

var PUBLIC_AUTH = "test123"

func Getprivatekey() (Response, error) {
	var obj Headerset
	var arrobj []Headerset
	var res Response

	con := db.CreateCon()

	sqlStatement := "SELECT * FROM tbl_collections"

	rows, err := con.Query(sqlStatement)
	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {

		err = rows.Scan(&obj.Privatekey)
		if err != nil {
			return res, err
		}

		arrobj = append(arrobj, obj)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = arrobj

	return res, nil
}

func Headersetmodel(c *fiber.Ctx) error {
	publickey := c.Get("Public")

	if publickey != "12345" {
		return c.Status(400).JSON("header public not working")
	}
	log.Println(publickey)
	return c.Next()
}

func IsPublic(Publickey string) (Response, error) {
	// var obj Headerset
	// var arrobj []Headerset
	var res Response

	cekstring := strings.Split(Publickey, " ")
	log.Println(Publickey)
	log.Println(cekstring)

	if len(cekstring) < 2 {
		res.Status = 400
		res.Message = "Invalid Authorization Set 1"
		// res.Data = arrobj
		return res, errors.New("cannot be empty")
	}

	log.Println(cekstring)
	log.Println(cekstring[0])

	reg, err := regexp.Compile("[^A-Za-z0-9] ")
	if err != nil {
		log.Fatal(err)
	}

	safe := reg.ReplaceAllString(cekstring[1], "-")
	// safe = strings.ToLower(strings.Trim(safe, "-"))

	decoded, err := base64.StdEncoding.DecodeString(safe)

	publickey := string(decoded)

	// if safe != "12345" {
	if publickey != PUBLIC_AUTH || cekstring[0] != "Public" {
		res.Status = 400
		res.Message = "Invalid Authorization Set 1"
		// res.Data = arrobj
		return res, errors.New("cannot be empty")
	}

	if publickey == "" {
		log.Println(cekstring)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	// res.Data = arrobj

	return res, nil
}

func IsPrivate(Privatekey string) (Response, error) {
	var obj Headerset
	var arrobj []Headerset
	var res Response

	cekstring := strings.Split(Privatekey, " ")

	if len(cekstring) < 2 {
		res.Status = 400
		res.Message = "Invalid Authorization Set 2"
		// res.Data = arrobj
		return res, errors.New("cannot be empty")
	}

	log.Println(cekstring)
	log.Println(cekstring[0])

	reg, err := regexp.Compile("[^A-Za-z0-9] ")
	if err != nil {
		log.Fatal(err)
	}

	safe := reg.ReplaceAllString(cekstring[1], "-")
	// safe = strings.ToLower(strings.Trim(safe, "-"))

	decoded, err := base64.StdEncoding.DecodeString(safe)

	privatekey := string(decoded)

	log.Println(privatekey)

	if string(cekstring[0]) == "Private" {
		con := db.CreateCon()

		log.Println("private")

		sqlStatement := "SELECT token FROM tbl__token where token = '" + privatekey + "'"

		rows, err := con.Query(sqlStatement)
		defer rows.Close()

		if err != nil {
			res.Status = 400
			res.Message = "Invalid Authorization Set 2"
			// res.Data = arrobj
			return res, errors.New("cannot be empty")
		}

		for rows.Next() {

			err = rows.Scan(&obj.Privatekey)
			if err != nil {
				res.Status = 400
				res.Message = "Invalid Authorization Set 2"
				// res.Data = arrobj
				return res, errors.New("cannot be empty")
			}

			arrobj = append(arrobj, obj)
		}

		log.Println(arrobj)

		// if safe != "12345" {
		if len(arrobj) == 0 || cekstring[0] != "Private" {
			res.Status = 400
			res.Message = "Invalid Authorization Set 2"
			// res.Data = arrobj
			return res, errors.New("cannot be empty")
		}

		res.Status = http.StatusOK
		res.Message = "Success"
		res.Data = arrobj

		return res, nil
	} else if string(cekstring[0]) == "Private-Web" {
		con := db.CreateCon()

		log.Println("private-web")

		sqlStatement := "SELECT external_web_token FROM tbl_token where external_web_token = '" + privatekey + "'"

		rows, err := con.Query(sqlStatement)
		defer rows.Close()

		if err != nil {
			res.Status = 400
			res.Message = "Invalid Authorization Set 3"
			// res.Data = arrobj
			return res, errors.New("cannot be empty")
		}

		for rows.Next() {

			err = rows.Scan(&obj.Privatekey)
			if err != nil {
				res.Status = 400
				res.Message = "Invalid Authorization Set 3"
				// res.Data = arrobj
				return res, errors.New("cannot be empty")
			}

			arrobj = append(arrobj, obj)
		}

		log.Println(arrobj)

		// if safe != "12345" {
		if len(arrobj) == 0 || cekstring[0] != "Private-Web" {
			res.Status = 400
			res.Message = "Invalid Authorization Set 3"
			// res.Data = arrobj
			return res, errors.New("cannot be empty")
		}

		res.Status = http.StatusOK
		res.Message = "Success"
		res.Data = arrobj

		return res, nil
	} else {

		log.Println("ga kemana mana")

		res.Status = 400
		res.Message = "Invalid Authorization Set 2"
		// res.Data = arrobj
		return res, errors.New("cannot be empty")
	}
}
