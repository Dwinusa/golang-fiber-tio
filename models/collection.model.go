package models

import (
	"net/http"

	validator "github.com/go-playground/validator/v10"

	"halalpedia-collection/db"
)

type Collection struct {
	CollectionID             int    `json:"collection_id"`
	CollectionIdentifier     string `json:"collection_identifier" validate:"required"`
	CollectionType           int    `json:"collection_type" validate:"required"`
	CollectionCriteria       int    `json:"collection_criteria" validate:"required"`
	CollectionPrimaryRecords int    `json:"collection_primary_records" validate:"required"`
	CollectionChildRecords   int    `json:"collection_child_records" validate:"required"`
	CollectionDisplayOrder   int    `json:"collection_display_order" validate:"required"`
	CollectionActive         int    `json:"collection_active" validate:"required"`
	CollectionDeleted        int    `json:"collection_deleted" validate:"required"`
	CollectionLinkURL        string `json:"collection_link_url" validate:"required"`
	CollectionLayoutType     int    `json:"collection_layout_type" validate:"required"`
}

func FetchAllCollection() (Response, error) {
	var obj Collection
	var arrobj []Collection
	var res Response

	con := db.CreateCon()

	sqlStatement := "SELECT * FROM tbl_collections"

	rows, err := con.Query(sqlStatement)
	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {

		err = rows.Scan(&obj.CollectionID, &obj.CollectionIdentifier, &obj.CollectionType, &obj.CollectionCriteria, &obj.CollectionPrimaryRecords, &obj.CollectionChildRecords, &obj.CollectionDisplayOrder, &obj.CollectionActive, &obj.CollectionDeleted, &obj.CollectionLinkURL, &obj.CollectionLayoutType)
		if err != nil {
			return res, err
		}

		arrobj = append(arrobj, obj)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = arrobj

	return res, nil
}

func StoreCollection(CollectionIdentifier string, CollectionType int, CollectionCriteria int, CollectionPrimaryRecords int, CollectionChildRecords int, CollectionDisplayOrder int, CollectionActive int, CollectionDeleted int, CollectionLinkUrl string, CollectionLayoutType int) (Response, error) {
	var res Response

	v := validator.New()

	peg := Collection{
		CollectionIdentifier:     CollectionIdentifier,
		CollectionType:           CollectionType,
		CollectionCriteria:       CollectionCriteria,
		CollectionPrimaryRecords: CollectionPrimaryRecords,
		CollectionChildRecords:   CollectionChildRecords,
		CollectionDisplayOrder:   CollectionDisplayOrder,
		CollectionActive:         CollectionActive,
		CollectionDeleted:        CollectionDeleted,
		CollectionLinkURL:        CollectionLinkUrl,
		CollectionLayoutType:     CollectionLayoutType,
	}

	err := v.Struct(peg)
	if err != nil {
		return res, err
	}

	con := db.CreateCon()

	sqlStatement := "INSERT tbl_collections (collection_identifier,collection_type,collection_criteria,collection_primary_records,collection_child_records,collection_display_order,collection_active,collection_deleted,collection_link_url,collection_layout_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(CollectionIdentifier, CollectionType, CollectionCriteria, CollectionPrimaryRecords, CollectionChildRecords, CollectionDisplayOrder, CollectionActive, CollectionDeleted, CollectionLinkUrl, CollectionLayoutType)
	if err != nil {
		return res, err
	}

	lastInsertedId, err := result.LastInsertId()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"last_inserted_id": lastInsertedId,
	}

	return res, nil
}

func UpdateCollection(CollectionIdentifier string, CollectionType int, CollectionCriteria int, CollectionPrimaryRecords int, CollectionChildRecords int, CollectionDisplayOrder int, CollectionActive int, CollectionDeleted int, CollectionLinkUrl string, CollectionLayoutType int, CollectionID int) (Response, error) {
	var res Response

	con := db.CreateCon()

	sqlStatement := "UPDATE tbl_collections SET collection_identifier = ?,collection_type = ?,collection_criteria = ?,collection_primary_records = ?,collection_child_records = ?,collection_display_order = ?,collection_active = ?,collection_deleted = ?,collection_link_url = ?,collection_layout_type = ? WHERE collection_id = ?"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(CollectionIdentifier, CollectionType, CollectionCriteria, CollectionPrimaryRecords, CollectionChildRecords, CollectionDisplayOrder, CollectionActive, CollectionDeleted, CollectionLinkUrl, CollectionLayoutType, CollectionID)
	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"rows_affected": rowsAffected,
	}

	return res, nil
}

func DeleteCollection(id int) (Response, error) {
	var res Response

	con := db.CreateCon()

	sqlStatement := "DELETE FROM tbl_collections WHERE collection_id = ?"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(id)
	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"rows_affected": rowsAffected,
	}

	return res, nil
}
