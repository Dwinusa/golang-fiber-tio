module halalpedia-collection

go 1.15

require (
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-playground/validator/v10 v10.4.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofiber/fiber/v2 v2.0.5
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
