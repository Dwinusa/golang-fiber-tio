package routes

import (
	"halalpedia-collection/controllers"
	"log"

	"github.com/gofiber/fiber/v2"
	// "github.com/labstack/echo"
)

func Headerset(c *fiber.Ctx) error {
	publickey := c.Get("Public")

	if publickey != "12345" {
		return c.Status(400).JSON("header public not working")
	}
	log.Println(publickey)
	return c.Next()
}

func Init() *fiber.App {
	e := fiber.New()

	e.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	testGroup := e.Group("/collection")

	testGroup.Get("/test", controllers.IsPublic, func(c *fiber.Ctx) error {
		return c.SendString(c.Get("Public"))
	})
	testGroup.Get("/col", controllers.IsPublic, controllers.FetchAllCollection)
	testGroup.Get("/collogin", controllers.Islogin, controllers.FetchAllCollection)

	// middleware.Headerset()

	testGroup.Get("/", controllers.FetchAllCollection)
	testGroup.Post("/", controllers.StoreCollection)
	testGroup.Put("/", controllers.UpdateCollection)
	testGroup.Delete("/", controllers.DeleteCollection)

	// e.GET("/generate-hash/:password", controllers.GenerateHashPassword)
	// e.POST("/login", controllers.CheckLogin)

	// e.GET("/test-struct-validation", controllers.TestStructValidation)
	// e.GET("/test-variable-validation", controllers.TestVariableValidation)

	// e.Use(basicauth.New(basicauth.Config{
	// 	Users: map[string]string{
	// 		"john":  "doe",
	// 		"admin": "123456",
	// 	},
	// 	Realm: "Forbidden",
	// 	Authorizer: func(user, pass string) bool {
	// 		if user == "john" && pass == "doe" {
	// 			return true
	// 		}
	// 		if user == "admin" && pass == "123456" {
	// 			return true
	// 		}
	// 		return false
	// 	},
	// 	Unauthorized: func(c *fiber.Ctx) error {
	// 		return c.Status(400).JSON("Tolong isikan authentication")
	// 	},
	// 	ContextUsername: "_user",
	// 	ContextPassword: "_pass",
	// }))

	// e.Use(basicauth.New(basicauth.Config{
	// c.Set("Content-Type", "text/plain")
	// => "Content-type: text/plain"
	// })
	return e
}
