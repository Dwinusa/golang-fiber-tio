package controllers

import (
	"log"
	"net/http"
	"strconv"

	"halalpedia-collection/models"

	"github.com/gofiber/fiber/v2"
)

func FetchAllCollection(c *fiber.Ctx) error {
	result, err := models.FetchAllCollection()
	if err != nil {
		// return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		return c.Status(400).JSON(http.StatusInternalServerError)
	}
	log.Println(result)
	return c.Status(200).JSON(result)
}

func StoreCollection(c *fiber.Ctx) error {
	collection_identifier := c.FormValue("collection_identifier")
	collection_type := c.FormValue("collection_type")
	collection_criteria := c.FormValue("collection_criteria")
	collection_primary_records := c.FormValue("collection_primary_records")
	collection_child_records := c.FormValue("collection_child_records")
	collection_display_order := c.FormValue("collection_display_order")
	collection_active := c.FormValue("collection_active")
	collection_deleted := c.FormValue("collection_deleted")
	collection_link_url := c.FormValue("collection_link_url")
	collection_layout_type := c.FormValue("collection_layout_type")

	convcollection_type, err := strconv.Atoi(collection_type)
	convcollection_criteria, err := strconv.Atoi(collection_criteria)
	convcollection_primary_records, err := strconv.Atoi(collection_primary_records)
	convcollection_child_records, err := strconv.Atoi(collection_child_records)
	convcollection_display_order, err := strconv.Atoi(collection_display_order)
	convcollection_active, err := strconv.Atoi(collection_active)
	convcollection_deleted, err := strconv.Atoi(collection_deleted)
	convcollection_layout_type, err := strconv.Atoi(collection_layout_type)

	result, err := models.StoreCollection(collection_identifier,
		convcollection_type,
		convcollection_criteria,
		convcollection_primary_records,
		convcollection_child_records,
		convcollection_display_order,
		convcollection_active,
		convcollection_deleted,
		collection_link_url,
		convcollection_layout_type)

	if err != nil {
		return c.JSON(http.StatusInternalServerError)
	}

	return c.JSON(result)
}

func UpdateCollection(c *fiber.Ctx) error {
	collection_id := c.FormValue("collection_id")
	collection_identifier := c.FormValue("collection_identifier")
	collection_type := c.FormValue("collection_type")
	collection_criteria := c.FormValue("collection_criteria")
	collection_primary_records := c.FormValue("collection_primary_records")
	collection_child_records := c.FormValue("collection_child_records")
	collection_display_order := c.FormValue("collection_display_order")
	collection_active := c.FormValue("collection_active")
	collection_deleted := c.FormValue("collection_deleted")
	collection_link_url := c.FormValue("collection_link_url")
	collection_layout_type := c.FormValue("collection_layout_type")

	convcollection_id, err := strconv.Atoi(collection_id)
	convcollection_type, err := strconv.Atoi(collection_type)
	convcollection_criteria, err := strconv.Atoi(collection_criteria)
	convcollection_primary_records, err := strconv.Atoi(collection_primary_records)
	convcollection_child_records, err := strconv.Atoi(collection_child_records)
	convcollection_display_order, err := strconv.Atoi(collection_display_order)
	convcollection_active, err := strconv.Atoi(collection_active)
	convcollection_deleted, err := strconv.Atoi(collection_deleted)
	convcollection_layout_type, err := strconv.Atoi(collection_layout_type)

	if err != nil {
		return c.JSON(http.StatusInternalServerError)
	}

	result, err := models.UpdateCollection(collection_identifier,
		convcollection_type,
		convcollection_criteria,
		convcollection_primary_records,
		convcollection_child_records,
		convcollection_display_order,
		convcollection_active,
		convcollection_deleted,
		collection_link_url,
		convcollection_layout_type,
		convcollection_id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError)
	}

	return c.JSON(result)
}

func DeleteCollection(c *fiber.Ctx) error {
	id := c.FormValue("id")

	conv_id, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError)
	}

	result, err := models.DeleteCollection(conv_id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError)
	}

	return c.JSON(result)
}
