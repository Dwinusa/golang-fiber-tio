package controllers

import (
	"log"
	"net/http"

	"halalpedia-collection/models"

	"github.com/gofiber/fiber/v2"
)

func Islogin(c *fiber.Ctx) error {
	publickey := c.Get("Public-Authorization")
	privatekey := c.Get("Private-Authorization")

	log.Println(publickey)
	result, err := models.IsPublic(publickey)
	log.Println(result)
	if err != nil {
		// return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		return c.Status(http.StatusBadRequest).JSON(result)
	}
	result, err = models.IsPrivate(privatekey)
	if err != nil {
		// return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		return c.Status(400).JSON(result)
	}
	log.Println(result)
	return c.Next()

	// return c.Status(200).JSON(result)
}

func IsPublic(c *fiber.Ctx) error {
	publickey := c.Get("Public-Authorization")

	result, err := models.IsPublic(publickey)
	log.Println(result)
	if err != nil {
		// return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
		return c.Status(http.StatusBadRequest).JSON(result)
	}
	return c.Next()
}

func Headersetmodel(c *fiber.Ctx) error {
	publickey := c.Get("Public")

	if publickey != "12345" {
		return c.Status(400).JSON("header public not working")
	}
	log.Println(publickey)
	return c.Next()
}
